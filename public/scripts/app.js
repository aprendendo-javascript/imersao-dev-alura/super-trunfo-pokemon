//Constants
const DEBUG = false;
const HIGHER_GENERATION = 8;
const BLINK_RATE = 700;
const POKEMON_CRY_VOLUME = 0.1;
const BASE_URL_TO_POKEMON_CRIES = "https://pokemoncries.com/cries/"; //https://pokemoncries.com/cries/{pokemonId}.mp3
const ALT_BASE_URL_TO_POKEMON_CRIES =
  "https://play.pokemonshowdown.com/audio/cries/"; //https://play.pokemonshowdown.com/audio/cries/{pokemonName}.mp3

const winnerPokemonAudio = document.querySelector("#winnerPokemonAudio");
const deckHeightLbl = document.querySelector("#deckHeightLbl");
const genChkBoxesHtml = [
  document.querySelector("#first-gen"),
  document.querySelector("#second-gen"),
  document.querySelector("#third-gen"),
  document.querySelector("#fourth-gen"),
  document.querySelector("#fifth-gen"),
  document.querySelector("#sixty-gen"),
  document.querySelector("#seventh-gen"),
  document.querySelector("#eighth-gen")
];
const scoreboardHtml = document.querySelector("#scoreboard");
const turnInfoHtml = document.querySelector("#turnInfo");
const turnInfoContainerHtml = document.querySelector("#turnInfoContainer");
const cardsDiv = document.querySelector("#cardsDiv");
const drawsBtn = document.querySelector("#drawsBtn");
const playBtn = document.querySelector("#playBtn");
const restartBtn = document.querySelector("#restartBtn");

//Variables
let generations = [];
let totalNumberOfPokemon;
let validPokemonIds = [];
let pokemonIdsDeck = [];

let mainDeck = [];
let playerDeck = [];
let machineDeck = [];
let tieStack = [];

let machinePokemon;
let playerPokemon;
let superTrunfoCardsId = [];
let combatCardsId = [];
let round;
let wins = 0;
let loses = 0;
let ties = 0;

//Functions
//API Consume
function requestGeneration(id) {
  let values;
  const urlToRequest = `https://pokeapi.co/api/v2/generation/${id}`;
  const request = new XMLHttpRequest();
  request.open("GET", urlToRequest, false);

  request.onload = function () {
    if (request.readyState == 4 && request.status == 200) {
      //Sucesso
      let ans = JSON.parse(request.responseText);
      values = ans;
    }
  };
  request.onerror = function () {
    console.log("Erro:" + request);
  };

  request.send();
  return values;
}
function requestPokemonInfo(search) {
  let values;
  const urlToRequest = `https://pokeapi.co/api/v2/pokemon/${search}`;
  const request = new XMLHttpRequest();
  request.open("GET", urlToRequest, false);

  request.onload = function () {
    if (request.readyState == 4 && request.status == 200) {
      //Sucesso
      let ans = JSON.parse(request.responseText);
      values = ans;
    }
  };
  request.onerror = function () {
    console.log("Erro:" + request);
  };

  request.send();
  return values;
}
//Specific purpose
const buildGenerationsArray = () => {
  totalNumberOfPokemon = 0;
  let oldInitId = 0;
  for (let i = 0; i < HIGHER_GENERATION; i++) {
    const generation = requestGeneration(i + 1);
    const initialId =
      i == 0 ? 1 : oldInitId + generations[i - 1].numberOfPokemonSpecies;
    const finalId = initialId + generation.pokemon_species.length - 1;

    generations.push({
      gen: generation,
      id: generation.id,
      numberOfPokemonSpecies: generation.pokemon_species.length,
      initialPokemonId: initialId,
      finalPokemonId: finalId
    });

    totalNumberOfPokemon += generations[i].numberOfPokemonSpecies;
    oldInitId = initialId;
  }
};
const updateScoreboard = () => {
  scoreboardHtml.innerHTML = `Rodada: ${round} Vitórias: ${wins}  Derrotas: ${loses} Empates: ${ties}`;
};
const updateDeckHeightLbl = () => {
  deckHeightLbl.innerHTML = `Cartas nos Deques => Jogador: ${playerDeck.length} Máquina: ${machineDeck.length} (Pilha Empates: ${tieStack.length})`;
};
const markCombatCardsId = (generation) => {
  for (let i = 0; i < parseInt(generations[generation].numberOfPokemonSpecies / 8); i++) {
    let randomIdInGeneration;
    let markedNewId;
    do {
      markedNewId = false;
      randomIdInGeneration = generations[generation].initialPokemonId + parseInt(Math.random() * (generations[generation].numberOfPokemonSpecies - 1));
      if (!combatCardsId.includes(randomIdInGeneration)) {
        combatCardsId.push(randomIdInGeneration);
        markedNewId = true;
      }
    } while (!markedNewId)
  }
};
const markSuperTrunfoCardsId = () => {
  for (let i = 0; i < generations.length; i++) {
    superTrunfoCardsId.push(generations[i].finalPokemonId);
    markCombatCardsId(i);
  }
};
const initializeValidPokemonIds = () => {
  validPokemonIds = Array(totalNumberOfPokemon + 1);
  for (let i = 0; i < genChkBoxesHtml.length; i++) {
    validPokemonIds.fill(
      genChkBoxesHtml[i].checked,
      generations[i].initialPokemonId,
      generations[i].finalPokemonId + 1
    );
    if (genChkBoxesHtml[i].checked) {
      addCardsToDecks(i);
    }
  }
  pokemonIdsDeck = Array(totalNumberOfPokemon + 1).fill(true);
  markSuperTrunfoCardsId();
};
function initRestartDeck() {
  drawsBtn.innerText = "Comprar Carta";
  drawsBtn.disabled = false;
  drawsBtn.focus();
  if (playerDeck.length != 0 && machineDeck.length != 0 && (wins != 0 || loses != 0 || ties != 0)) {
    showEndGameAlert(100);
  }
  turnInfoContainerHtml.innerHTML =
    '<h2 id="turnInfo" class="animate__animated animate__heartBeat">Turno do Jogador</h2>';
  cardsDiv.innerHTML = "";
  restartBtn.disabled = true;
  playBtn.disabled = true;
  pokemonIdsDeck.fill(true);
  round = 0;
  wins = 0;
  loses = 0;
  ties = 0;
  redistributeTheCards();
  updateDeckHeightLbl();
  updateScoreboard();
}
const redistributeTheCards = () => {
  while (playerDeck.length != 0) {
    mainDeck.push(playerDeck.pop());
  }
  while (machineDeck.length != 0) {
    mainDeck.push(machineDeck.pop());
  }
  while (tieStack.length != 0) {
    mainDeck.push(tieStack.pop());
  }
  shuffleDeck(mainDeck);
  dealTheCards(mainDeck);
}
const shuffleDeck = (deck) => {
  for (let i = deck.length - 1; i > 0; i--) {
    const j = Math.random() * (i + 1);
    deck.splice(i, 0, deck.splice(j, 1, deck[i])[0]);
    deck.splice(j, 1);
  }
};
const dealTheCards = (sourceDeck) => {
  while (sourceDeck.length > 1) {
    playerDeck.push(sourceDeck.pop());
    machineDeck.push(sourceDeck.pop());
  }
  if (sourceDeck.length != 0) {
    tieStack.push(sourceDeck.pop());
  }
  shuffleDeck(playerDeck);
  shuffleDeck(machineDeck);
};
const addCardsToDecks = (generation) => {
  for (let i = generations[generation].initialPokemonId; i < generations[generation].finalPokemonId + 1; i++) {
    mainDeck.push(i);
  }
  shuffleDeck(mainDeck);
  dealTheCards(mainDeck);
};
const removeCardsFromDeck = (deck) => {
  for (let i = deck.length - 1; i >= 0; i--) {
    if (!validPokemonIds[deck[i]]) {
      deck.splice(i, 1);
    }
  }
};
const removeCardsFromDecks = () => {
  removeCardsFromDeck(playerDeck);
  removeCardsFromDeck(machineDeck);
  removeCardsFromDeck(tieStack);
  shuffleDeck(playerDeck);
  shuffleDeck(machineDeck);
};
const generalSetup = () => {
  winnerPokemonAudio.volume = POKEMON_CRY_VOLUME;
  buildGenerationsArray();
  initializeValidPokemonIds();
  initRestartDeck();
};
const evalValidGeneration = (chkBox) => {
  for (let i = 0; i < genChkBoxesHtml.length; i++) {
    if (chkBox === genChkBoxesHtml[i]) {
      validPokemonIds.fill(
        chkBox.checked,
        generations[i].initialPokemonId,
        generations[i].finalPokemonId + 1
      );
      if (chkBox.checked) {
        addCardsToDecks(i);
      } else {
        removeCardsFromDecks();
      }
      break;
    }
  }
  updateDeckHeightLbl();
};
const showEndGameAlert = (delay) => {
  const msgConfiguration = {
    icon: null,
    title: null,
    text: null
  };
  if (machineDeck.length == 0 || (playerDeck.length != 0 && wins > loses)) {
    msgConfiguration.icon = "success";
    msgConfiguration.title =
      (playerDeck.length != 0 && machineDeck.length != 0 ? "Jogo Encerrado\n" : "Fim de Jogo\n") +
      "Você ganhou!!!";
  } else if (playerDeck.length == 0 || wins < loses) {
    msgConfiguration.icon = "error";
    msgConfiguration.title =
      (playerDeck.length != 0 && machineDeck.length != 0 ? "Jogo Encerrado\n" : "Fim de Jogo\n") + "Você perdeu";
  } else {
    msgConfiguration.icon = "info";
    msgConfiguration.title =
      (playerDeck.length != 0 && machineDeck.length != 0 ? "Jogo Encerrado\n" : "Fim de Jogo\n") +
      "Houve um empate";
  }
  msgConfiguration.text = `Obteve ${wins} vitórias, ${loses} derrotas e ${ties} empates.`;
  setTimeout(() => {
    Swal.fire(msgConfiguration);
  }, delay);
};

const popCardFrom = (deck) => {
  const pokemon = requestPokemonInfo(deck.pop());
  return {
    apiInfo: pokemon,
    cryUrl: getPokemonCryUrl(pokemon),
    card: null
  };
}
const unshiftCardsTo = (cards, deck) => {
  while (cards.length != 0) {
    deck.unshift(cards.pop());
  }
}

const aleatorySelectPokemon = () => {
  let randomPokemonId;
  do {
    randomPokemonId = parseInt(Math.random() * totalNumberOfPokemon) + 1;
  } while (
    !validPokemonIds[randomPokemonId] ||
    !pokemonIdsDeck[randomPokemonId]
  );
  pokemonIdsDeck[randomPokemonId] = false;
  const pokemon = requestPokemonInfo(randomPokemonId);
  return {
    apiInfo: pokemon,
    cryUrl: getPokemonCryUrl(pokemon)
  };
};
const buildPokemonStatsTable = (pokemon, selectStat) => {
  const statusTable = document.createElement("table");
  const tableHead = document.createElement("thead");
  const tableBody = document.createElement("tbody");

  tableHead.innerHTML =
    '<tr name="headRow"><th>  </th><th>Atributo</th><th>Valor</th></tr>';

  let statsHtml = "";
  for (let i = 0; i < pokemon.stats.length; i++) {
    const statsName = pokemon.stats[i].stat.name;
    const statsBaseStat = pokemon.stats[i].base_stat;
    statsHtml +=
      "<tr" +
      (selectStat === statsName ? " name='selected'" : "") +
      ">" + '<td> <input type="radio" name="stats' + (pokemon == playerPokemon.apiInfo ? "P" : "M") + '" value="' +
      statsName +
      '"' + (selectStat === statsName ? " checked " : "") + (selectStat !== undefined ? " disabled " : "") + '> </td>' +
      "<td>" +
      statsName +
      "</td><td>" +
      statsBaseStat +
      "</td></tr>";
  }
  tableBody.innerHTML = statsHtml;
  statusTable.appendChild(tableHead);
  statusTable.appendChild(tableBody);
  return statusTable;
};
const buildPokemonCard = (pokemon, selectStat) => {
  const pokemonBox = document.createElement("div");
  const pokemonImage = document.createElement("img");
  const pokemonName = document.createElement("h4");
  const pokemonStatusTable = buildPokemonStatsTable(pokemon, selectStat);
  let specialMarkerDiv = document.createElement("div");

  const name = pokemon.name;
  const id = pokemon.id;

  if (superTrunfoCardsId.includes(id)) {
    specialMarkerDiv.className = "star";
    specialMarkerDiv.title = "Super Trunfo";
  } else if (combatCardsId.includes(id)) {
    specialMarkerDiv.className = "combat-star";
    specialMarkerDiv.title = "Enfrenta o Super Trunfo";
  }
  const pokemonInfo = requestPokemonInfo(id);
  pokemonBox.className = "poster-box-sel" + (pokemon === playerPokemon.apiInfo ? " animate__animated animate__backInUp" : " animate__animated animate__backInDown");
  pokemonBox.id = name.charAt(0).toUpperCase() + name.slice(1);
  pokemonImage.src =
    pokemonInfo.sprites.other["official-artwork"].front_default;
  pokemonImage.className = "poster";
  pokemonName.innerText = `#${pokemonInfo.id}\n${name
    .charAt(0)
    .toUpperCase()}${name.slice(1)}`;
  pokemonName.className = "poster-title";

  pokemonBox.appendChild(specialMarkerDiv);
  pokemonBox.appendChild(pokemonImage);
  pokemonBox.appendChild(pokemonName);
  pokemonBox.appendChild(pokemonStatusTable);
  return pokemonBox;
};
const showPlayerCard = (selectStat) => {
  playerPokemon.card = buildPokemonCard(playerPokemon.apiInfo, selectStat)
  cardsDiv.appendChild(playerPokemon.card);
  playerPokemon.card.addEventListener('animationend', () => {
    winnerPokemonAudio.src = playerPokemon.cryUrl;
    winnerPokemonAudio.play();
  });
};
const getPokemonCryUrl = (pokemon) => {
  if (pokemon !== undefined && pokemon !== null) {
    if (pokemon.id < 803 || (pokemon.id > 809 && pokemon.id < 890)) {
      return `${BASE_URL_TO_POKEMON_CRIES}${pokemon.id}.mp3`;
    } else {
      return `${ALT_BASE_URL_TO_POKEMON_CRIES}${pokemon.name}.mp3`;
    }
  } else {
    return undefined;
  }
};
const disableRadios = (pokemonCard) => {
  const selectedStatRadios = pokemonCard.querySelectorAll("input");
  for (let i = 0; i < selectedStatRadios.length; i++) {
    selectedStatRadios[i].disabled = true;
  }
};
const showResult = (selectedStats) => {
  const playerStatsValue = playerPokemon.apiInfo.stats.find(
    (e) => e.stat.name === selectedStats
  ).base_stat;
  const machineStatsValue = machinePokemon.apiInfo.stats.find(
    (e) => e.stat.name === selectedStats
  ).base_stat;

  const machineCard = buildPokemonCard(machinePokemon.apiInfo, selectedStats);
  machinePokemon.card = machineCard;

  cardsDiv.appendChild(machineCard);

  let winner;
  let loser;

  if (
    (superTrunfoCardsId.includes(playerPokemon.apiInfo.id) &&
      superTrunfoCardsId.includes(machinePokemon.apiInfo.id)) ||
    (!superTrunfoCardsId.includes(playerPokemon.apiInfo.id) &&
      !superTrunfoCardsId.includes(machinePokemon.apiInfo.id)) ||
    (superTrunfoCardsId.includes(playerPokemon.apiInfo.id) &&
      combatCardsId.includes(machinePokemon.apiInfo.id)) ||
    (combatCardsId.includes(playerPokemon.apiInfo.id) &&
      superTrunfoCardsId.includes(machinePokemon.apiInfo.id))
  ) {
    if (playerStatsValue > machineStatsValue) {
      winner = playerPokemon;
      loser = machinePokemon;
      wins++;
    } else if (playerStatsValue < machineStatsValue) {
      winner = machinePokemon;
      loser = playerPokemon;
      loses++;
    } else {
      ties++;
    }
  } else if (superTrunfoCardsId.includes(playerPokemon.apiInfo.id)) {
    winner = playerPokemon;
    loser = machinePokemon;
    wins++;
  } else {
    winner = machinePokemon;
    loser = playerPokemon;
    loses++;
  }

  playerPokemon.card.className = "poster-box";
  machineCard.className = "animate__animated animate__backInDown poster-box";
  machineCard.addEventListener('animationend', () => {
    const cardsInDispute = [playerPokemon.apiInfo.id, machinePokemon.apiInfo.id];
    if (winner !== undefined) {
      winner.card.className = "poster-box-staccato";
      loser.card.className = "poster-box";

      winnerPokemonAudio.src = winner.cryUrl;
      winnerPokemonAudio.play();

      while (tieStack.length != 0) {
        cardsInDispute.push(tieStack.pop());
      }
      if (winner === playerPokemon) {
        unshiftCardsTo(cardsInDispute, playerDeck);
      } else {
        unshiftCardsTo(cardsInDispute, machineDeck);
      }
    } else {
      playerPokemon.card.className = "poster-box";
      machinePokemon.card.className = "poster-box";
      unshiftCardsTo(cardsInDispute, tieStack);
    }
  });
  updateScoreboard();
  disableRadios(playerPokemon.card);
  disableRadios(machinePokemon.card);
};
const machineSelectStats = () => {
  let selectedStats = machinePokemon.apiInfo.stats[0];
  for (let i = 1; i < machinePokemon.apiInfo.stats.length; i++) {
    if (machinePokemon.apiInfo.stats[i].base_stat > selectedStats.base_stat) {
      selectedStats = machinePokemon.apiInfo.stats[i];
    }
  }
  return selectedStats.stat.name;
};
function drawsACard() {
  cardsDiv.innerHTML = "";
  round++;
  updateScoreboard();
  machinePokemon = popCardFrom(machineDeck);
  playerPokemon = popCardFrom(playerDeck);
  updateDeckHeightLbl();
  if (DEBUG) {
    console.log(
      `Id: ${machinePokemon.apiInfo.id} Name: ${machinePokemon.apiInfo.name}`
    );
    console.log(
      `Id: ${playerPokemon.apiInfo.id} Name: ${playerPokemon.apiInfo.name}`
    );
  }
  if (round % 2 != 0) {
    if (round != 1) {
      turnInfoContainerHtml.innerHTML = '<h2 id="turnInfo" class="animate__animated animate__heartBeat">Turno do Jogador</h2>';
    }
    showPlayerCard();
    drawsBtn.disabled = true;
    drawsBtn.innerText = "Comprar Nova Carta";
    playBtn.disabled = false;
  } else {
    turnInfoContainerHtml.innerHTML = '<h2 id="turnInfo" class="animate__animated animate__heartBeat">Turno da Máquina</h2>';
    const selectedStat = machineSelectStats();
    showPlayerCard(selectedStat);
    //play();
  }
  drawsBtn.disabled = true;
  playBtn.disabled = false;
}
function play() {
  const selectedStatRadios = Array.from(
    cardsDiv.querySelectorAll('[name="statsP"]')
  ).filter((e) => e.checked == true);
  if (selectedStatRadios.length != 0) {
    selectedStatRadios[0].parentNode.parentNode.setAttribute(
      "name",
      "selected"
    );
    showResult(selectedStatRadios[0].value);
    if (playerDeck.length != 0 && machineDeck.length != 0) {
      drawsBtn.disabled = false;
      drawsBtn.focus();
    } else {
      showEndGameAlert(1000);
    }
    playBtn.disabled = true;
    restartBtn.disabled = false;
  } else {
    playerPokemon.card.className = "poster-box-sel animate__animated animate__wobble";
  }
}

//Code
generalSetup();